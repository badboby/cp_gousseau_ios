//
//  CPMapViewDelegateProxy.swift
//  ChauffeurPrivé_Gousseau_iOS
//
//  Created by Sébastien Gousseau on 07/02/2017.
//  Copyright © 2017 Sébastien Gousseau. All rights reserved.
//

import CoreLocation
import Mapbox

#if !RX_NO_MODULE
    import RxSwift
    import RxCocoa
#endif

class MapboxDelegateProxy : DelegateProxy, MGLMapViewDelegate, DelegateProxyType {
    
    class func currentDelegateFor(_ object: AnyObject) -> AnyObject? {
        let map: CPMapView = object as! CPMapView
        return map.delegate
    }
    
    class func setCurrentDelegate(_ delegate: AnyObject?, toObject object: AnyObject) {
        let map: CPMapView = object as! CPMapView
        map.delegate = delegate as? MGLMapViewDelegate
    }
}
