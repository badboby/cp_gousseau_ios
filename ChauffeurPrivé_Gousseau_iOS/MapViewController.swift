//
//  ViewController.swift
//  ChauffeurPrivé_Gousseau_iOS
//
//  Created by Sébastien Gousseau on 07/02/2017.
//  Copyright © 2017 Sébastien Gousseau. All rights reserved.
//

import UIKit
import Mapbox
import MapKit
import Toast_Swift

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: CPMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let auth    = TrackerService.instance.authorized.asObservable()
        let loc     = TrackerService.instance.location.asObservable().take(1)
        
        //Permet de filtrer un refus d'autorisation du service de localisation et d'afficher un Toast
        _ = auth.distinctUntilChanged().subscribe(onNext: { authorized in
            if !authorized { self.view.makeToast("Votre service de localisation est désactivé. Cette application ne pourra pas fonctionner correctement.", duration: 5.0, position: .top) }
        })
        
        //Permet de récupérer la première localisation de l'utilisateur et d'afficher un PIN
        _ = loc.subscribe(onNext: { (coordinate) in
            self.mapView.addAnnotationForCoordinate(coordinate: coordinate)
            self.mapView.centerCoordinate   = coordinate
            self.mapView.zoomLevel          = 10.0
        })
        
        _ = mapView.rx.didChangeRegion.subscribe(onNext: { _ in
            print("regionChanged")
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

