//
//  MapBoxEx.swift
//  ChauffeurPrivé_Gousseau_iOS
//
//  Created by Sébastien Gousseau on 07/02/2017.
//  Copyright © 2017 Sébastien Gousseau. All rights reserved.
//

import UIKit
import Mapbox
import RxSwift
import RxCocoa

protocol CPMapViewDelegate {
    func addAnnotationForCoordinate(coordinate: CLLocationCoordinate2D)
}

class CPMapView: MGLMapView, MGLMapViewDelegate, CPMapViewDelegate {

    override init(frame: CGRect) {
        super.init(frame: frame, styleURL: nil)
    }
    
    override init(frame: CGRect, styleURL: URL?) {
        super.init(frame: frame, styleURL: styleURL)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addAnnotationForCoordinate(coordinate: CLLocationCoordinate2D) {
        let point = MGLPointAnnotation()
        point.coordinate = coordinate
        self.addAnnotation(point)
    }
}

extension Reactive where Base: CPMapView {
    
    var delegate: DelegateProxy {
        return MapboxDelegateProxy.proxyForObject(base)
    }
    
    /**
     Reactive wrapper for `delegate` message mapView(_ mapView: MGLMapView, didAdd annotationViews: [MGLAnnotationView]).
     */
    var didAdd: Observable<[MGLAnnotationView]> {
        return delegate.methodInvoked(#selector(MGLMapViewDelegate.mapView(_:didAdd:))).map { a in return a[1] as! [MGLAnnotationView] }
    }
    
    //optional public func mapView(_ mapView: MGLMapView, didFailToLocateUserWithError error: Error)
    var didFailToLocateUser: Observable<Error> {
        return delegate.methodInvoked(#selector(MGLMapViewDelegate.mapView(_:didFailToLocateUserWithError:))).map { a in return a[1] as! Error }
    }
    
    var didChangeRegion: Observable<Void> {
        return delegate.methodInvoked(#selector(MGLMapViewDelegate.mapView(_:regionDidChangeAnimated:))).map { a in return }
    }
}
